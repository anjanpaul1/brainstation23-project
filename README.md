# Images of Kubernetes Cluster and Output

![Nodes Details](https://gitlab.com/anjanpaul1/brainstation-task/-/blob/main/BrainstationNode.png)

![Pods Details](https://gitlab.com/anjanpaul1/brainstation-task/-/blob/main/Brainstaton%20pods.png)

## i tried to secure our application from outer side. For this reason I deployed the Application services with ClusterIP.

![Services Details](https://gitlab.com/anjanpaul1/brainstation-task/-/blob/main/Bainstation%20svc%20wide.png)

![Output Details](https://gitlab.com/anjanpaul1/brainstation-task/-/blob/main/Screenshot%20from%202022-01-07%2016-08-13.png)
